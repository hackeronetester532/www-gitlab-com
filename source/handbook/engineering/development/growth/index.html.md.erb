---
layout: handbook-page-toc
title: Growth Section
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## FY2021 Direction

Develop a respectful and privacy focussed data collection framework that allows us to make informed product decisions which improve the way our customers sign up to GitLab, manage and renew their licenses, and upgrade their accounts to higher tiers.

## Mission

The Growth section consists of groups that eliminate barriers between our users and our product value.

**Managing customer licensing and transactions**

The [Fulfillment](/handbook/engineering/development/growth/fulfillment/) Group is responsible for:

Licensing
- Seamless license compliance for customers and GitLab Team Members

Transactions
- Make doing business with GitLab  efficient, easy, and a terrific user experience

**Deliver telemetry data that improves our product**

The [Telemetry](/handbook/engineering/development/growth/telemetry/) Group is responsible for:

Collection
- Collect information from GitLab.com and self-managed instances to improve our product
- Respect our users' privacy

Analysis
- Analyze and visualize collected data
- Operationalize what we’re collecting for the benefit of GitLab and our customers


**Drive value for the business and our users by improving activation, retention, upsell, and per-stage adoption**

We focus on validating ideas with data across the following four Groups:

[Acquisition Group](/handbook/engineering/development/growth/acquisition-conversion/)

- Get users off to a successful start
- Increase users completing key actions

[Conversion Group](/handbook/engineering/development/growth/acquisition-conversion/)
- Encourage cross-stage usage
- Increase Stage Monthly Active Users

[Expansion Group](/handbook/engineering/development/growth/expansion/)
- Drive adoption of “higher” tiers
- Increase revenue per user/transaction

[Retention Group](/handbook/engineering/development/growth/retention/)
- Get customers and users to return
- Increase gross retention

We work closely with the [Data Team](/handbook/business-ops/data-team/) along with our [Product Team](/handbook/product/categories/#growth-stage)
counterparts to design and implement experiments that measure the impact of changes to our messaging, UX, and overall experience of using GitLab.

## September 2019 Fast Boot

The Acquisition, Expansion, Conversion and Retention groups took part in a [Fast Boot](/handbook/engineering/fast-boot/) in September 2019.
[The planning issue](https://gitlab.com/gitlab-org/growth/product/issues/1) contains the proposal for Fast Boot,
and outcomes are available in the [Growth Fast Boot Page](/handbook/engineering/development/growth/fast-boot).

## Section Members

The following people are permanent members of the Growth Section:

<%=
departments = ['Acquisition', 'Conversion', 'Expansion' , 'Retention', 'Fulfillment', 'Telemetry', 'Static Site Editor']
department_regexp = /(#{Regexp.union(departments)})/

direct_team(role_regexp: department_regexp, manager_role: 'Director of Engineering, Growth')
%>

## All Team Members

The following people are permanent members of groups that belong to the Growth Section:

### Acquisition
<%= department_team(base_department: "Acquisition Team") %>

### Conversion
<%= department_team(base_department: "Conversion Team") %>

### Expansion
<%= department_team(base_department: "Expansion Team") %>

### Retention
<%= department_team(base_department: "Retention Team") %>

### Fulfillment Back End
<%= department_team(base_department: "Fulfillment BE Team") %>

### Fulfillment Front End
<%= department_team(base_department: "Fulfillment FE Team") %>

### Telemetry Back End
<%= department_team(base_department: "Telemetry BE Team") %>

### Telemetry Front End
<%= department_team(base_department: "Telemetry FE Team") %>


## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%=
role_regexp = /[,&] (Growth|Fulfillment|Telemetry|Static Site Editor|Create:Static Site Editor)/
direct_manager_role = 'Director of Engineering, Growth'
other_manager_roles = [
  'Engineering Manager, Growth:Expansion and Retention',
  'Engineering Manager, Growth:Acquisition and Conversion',
  'Engineering Manager, Fulfillment',
  'Frontend Engineering Manager, Fulfillment'
]
stable_counterparts(role_regexp: role_regexp, direct_manager_role: direct_manager_role, other_manager_roles: other_manager_roles)
%>


## How We Work
As part of the wider Growth stage we track and work on issues with the label `~"devops::growth"`.

### Growth Issues Board
The [Growth issues board] shows `~"devops::growth"` issues by workflow state,
and can be filtered by additional `group::` label:
* [Growth issues board] unfiltered
* [`~"growth::acquisition"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aacquisition)
* [`~"growth::conversion"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aconversion)
* [`~"growth::expansion"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aexpansion)
* [`~"growth::retention"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aretention)
* [`~"growth::fulfillment"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Afulfillment)
* [`~"growth::telemetry"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Atelemetry)

This board uses the [Product Development Workflow](/handbook/engineering/workflow/#workflow-labels) labels for issues where applicable.

## Running Experiments

### Experiment Issue Boards
Experiments are tracked on Growth - Experiments boards by group:

| Description | gitlab-org | gitlab-com | gitlab-services |
| ------ | ------ | ------ | ------ |
| Growth experiments by label | [Growth - Experiments](https://gitlab.com/groups/gitlab-org/-/boards/1352542) | [Growth - Experiments](https://gitlab.com/groups/gitlab-com/-/boards/1542208) | [Growth - Experiments](https://gitlab.com/groups/gitlab-services/-/boards/1542265) |

### Experiment Issue Creation
All growth experiments consist of two issues:
* **Experiment issue**: this issue acts as the single source of truth for an experiment which includes an experiment overview, the rollout plan, and the results. This issue will be tagged with the `~"growth experiment"` label, and a scoped `experiment::` label.
* **Clean up issue**: this issue is used to clean up an experiment after an experiment is completed. The clean up work may include completely removing the experiment or refactoring the experiment feature for the long run.

### Experiment Issue Labels
For tracking the status of experiments we also use the following scoped `experiment::` labels:
* `~"experiment::active"`
* `~"experiment::blocked"`
* `~"experiment::validated"`
* `~"experiment::invalidated"`
* `~"experiment::inconclusive"`

### UX Workflows
We follow the UX Team's [Product Designer](/handbook/engineering/ux/ux-designer/#product-designer)
and [Researcher](/handbook/engineering/ux/ux-research/) workflows.
We also have a Growth specific workflow that you can read about [here](/handbook/engineering/ux/stage-group-ux-strategy/growth/#how-we-work).

#### A note about visual reviews of MRs

The engineering team applies the `UX` label to any MR that introduces a visual, interaction or flow change. These MRs can be related to new issues, bugs, followups, or any type of MR. If the engineer isn't sure whether the MR needs UX, they should consult the designer who worked on the related issue, and/or the designer assigned to that stage group, or the UX manager.

Visual reviews are required for any MR with the `UX` label. When the MR is in `workflow::In review`, the engineer assigns the MR to the designer for a visual review. This can happen in parallel with the maintainer review, but designers should prioritize these reviews to complete them as quickly as possible.

There are times when it isn't possible or practical for a designer to complete their visual review via Review Apps or GDK. At these times the designer and engineer should coordinate a demo.

## Common Links

* [Growth section]
* [Growth issues board]
* `#s_growth` in [Slack](https://gitlab.slack.com/archives/s_growth)
* [Growth Performance Indicators]
* [Fulfillment issues board]
* `#g_fulfillment` in [Slack](https://gitlab.slack.com/archives/g_fulfillment)
* [Telemetry issues board]
* `#g_telemetry` in [Slack](https://gitlab.slack.com/archives/g_telemetry)
* [Growth opportunities]
* [Growth meetings and agendas]
* [GitLab values]

[GitLab values]: /handbook/values/
[Growth section]: /handbook/engineering/development/growth/
[Growth issues board]: https://gitlab.com/groups/gitlab-org/-/boards/1158847
[Growth opportunities]: https://gitlab.com/gitlab-org/growth/product
[Growth meetings and agendas]: https://docs.google.com/document/d/1QB9uVQQFuKhqhPkPwvi48GaibKDwGAfKKqcF-s3Y6og

[Growth Performance Indicators]: /handbook/engineering/development/growth/performance-indicators/

[Fulfillment issues board]: https://gitlab.com/gitlab-org/fulfillment/issues
[Telemetry issues board]: https://gitlab.com/gitlab-org/telemetry/issues
