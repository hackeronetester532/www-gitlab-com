---
layout: markdown_page
title: "Usecase: Continuous Integration"
---

<!--

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
-->

## Continuous Integration

The Continuous Integration (CI) usecase is a staple of modern software development in the digital age. It's unlikely that you hear the word "DevOps" without a reference to "Continuous Integration and Continuous Delivery" (CI/CD) soon after. In the most basic sense, the CI part of the equation enables development teams to automate building and testing their code.

When practicing CI, teams collaborate on projects by using a shared repository to store, modify and track frequent changes to their codebase. Developers check in, or integrate, their code into the repository multiple times a day and rely on automated tests to run in the background. These automated tests verify the changes by checking for potential bugs and security vulnerabilities, as well as performance and code quality degradations. Running tests as early in the software development lifecycle as possible is advantageous in order to detect problems before they intensify.

CI makes software development easier, faster, and less risky for developers. By automating builds and tests, developers can make smaller changes that are easier to reason about and commit them with confidence. They get earlier feedback on their code in order to iterate and improve it quickly increasing the overall pace of innovation.  

[GitLab CI](/product/continuous-integration/) comes built-in to GitLab's complete DevOps platform delivered as a single application. There's no need to cobble together multiple tools and users get a seamless experience out-of-the-box.

<!--
facts about approaching 100mil software developers...
-->

## Personas

### User Persona

The typical **user personas** for this usecase are the Developer, Development team lead, and DevOps engineer.

**Software Developer [Sacha](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)**
- I'm a problem solver, a critical thinker, and I love to learn. I work best on planned tasks and want to spend a majority of my time writing code that ends up being delivered to customers in the form of lovable features.
- I have expertise in all sorts of development tools and programming languages that make me an extremely valuable resource in the DevOps space.

**Development Team Lead [Delaney](/marketing/product-marketing/roles-personas/#delaney-development-team-lead)**
- wip

**DevOps Engineer [Devon](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)**
- wip


### Buyer Personas

CI purchasing typically does not require executive involvement. It is usually acquired and installed via our freemium offering without procurement or IT's approval. This process is commonly known as shadow IT. When the upgrade is required the [VP of IT](/handbook/marketing/product-marketing/roles-personas/#buyer-personas) is the most frequent decision-maker. The influence of the [VP Application Development](/handbook/marketing/product-marketing/roles-personas/#buyer-personas) is notable too.

Analyst Coverage

## Analyst Coverage

- [Forrester Wave for Cloud-Native CI Tools](/analysts/forrester-cloudci19/)
- [Forrester Continuous Integration Tools](/analysts/forrester-ci/)

## UseCase Capabilities

tbd

## Top 3 Differentiators

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
| **Leading SCM and CI in one application** | PLACEHOLDER | **-** PLACEHOLDER |
| **Built in security and compliance** | PLACEHOLDER | **-** PLACEHOlDER |
| **Rapid innovation** | PLACEHOLDER | **-** PLACEHOLDER |


## Competitive Comparison
Amongst the many competitors in the DevOps space, [Jenkins](/devops-tools/jenkins-vs-gitlab.html) and [CircleCI](/devops-tools/circle-ci-vs-gitlab.html) are the closest competitors offering continuous integration capabilities.

## Customer References and Case Studies
* [How Jaguar Land Rover embraced CI to speed up their software lifecycle](/blog/2018/07/23/chris-hill-devops-enterprise-summit-talk/)

* [Goldman Sachs improves from 1 build every two weeks to over a thousand per day](/customers/goldman-sachs/)

* [GitLab CI supports Ticketmaster's ramp up to weekly mobile releases](/blog/2017/06/07/continous-integration-ticketmaster/)

## Resources

### Presentations
* [tbd]()

### Continuous Integration Videos
* [tbd]()

### Integrations Demo Videos
* [tbd]()

### Clickthrough & Live Demos
* [tbd]()
