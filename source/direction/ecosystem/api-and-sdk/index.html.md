---
layout: markdown_page
title: "Category Direction - SDKs & Integration APIs"
---

- TOC
{:toc}

| ------ | ------ |
| **Stage** | [Enablement](https://about.gitlab.com/direction/enablement) | 
| **Maturity** | [Viable](https://about.gitlab.com/handbook/product/categories/maturity/) |

- [Issue List](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AAPIs%2FSDKs)
- [Overall Vision](/direction/ecosystem/)

This vision is a work in progress, and everyone can contribute:

* Please comment, thumbs-up (or down!), and contribute to the linked issues and epics on this category page. Sharing your feedback directly on GitLab.com is the best way to contribute to our vision.
* Please share feedback directly via [email](mailto:pdeuley@gitlab.com), [Twitter](https://twitter.com/gitlab) or on a video call. If you're a GitLab user or developer and have direct knowledge of your need from our API and SDK, we'd especially love to hear from you.

## Overview

<!-- where we are now -->

GitLab's **SDKs & Integration APIs** category provides all the tools and resources for developers to build upon GitLab as a platform. While our [Integrations category](/direction/ecosystem/integrations/) support specific, key integrations, the API & SDK category was created in support of _all other integration needs_.

Currently, a simple and powerful API is available for developers to use to integrate with GitLab. Learn more about our API in [our developer documentation](https://docs.gitlab.com/ee/api/). 

Additionally, community created-and-maintained [API Clients](https://about.gitlab.com/partners/#api-clients) and [CLI Clients](https://about.gitlab.com/partners/#cli-clients) are available.

## Direction

<!--
What we want to achieve (without regard to resources)
Why is this important
-->

As the GitLab community grows, and they increasingly rely on GitLab as a central part of their toolchain, it is critical that we provide support for integrating our product with a wider variety of systems. These integrations could be with project management systems, ERPs, custom dashboards and reporting systems, or any other type of custom-built tool. 

Additionally, providing these tools and resources is vital to partners and other 3rd party systems that want to integrate _their products_ with GitLab.

To serve these needs, GitLab needs to provide a _best-in-class_ developer experience by having:

* An expanded set of API endpoints that allow access to more functionality across the product
* Client libraries that support the most commonly used languages and frameworks (such as Python, Ruby, Javascript, and Go)
* CLI tools that offer scriptable/interactive usage of GitLab
* Robust developer documentation that covers all of these features and tooling
* Reference applications and example code that help developers hit the ground running

By providing powerful and flexible tools, we allow developers to consume GitLab _as a platform_--giving them the freedom to create anything they can imagine. [Penflip](https://www.penflip.com/) is a great example of a novel product that was built upon GitLab that uses the core GitLab functionality to produce a whole new experience. These developers took GitLab and reimagined it for a wholly different purpose and a totally different type of user. 

Making room for this sort of innovation and creativity will let us grow our community and impact in novel ways, and the more we expand the resources we provide, the more of this impact we will see.

## Maturity

Today, we consider our **API and SDK** to be [Viable](/direction/maturity/). Below is how we think about how we'll grow that maturity level over time:

* **Viable** API and SDKs give developers access to the majority of core GitLab functionality and offer basic documentation covering the usage of the API. _(Where we are today)_
* **Complete** API and SDKs give developers access to almost all GitLab functionality through direct API access, and coverage of the majority of functionality through native client libraries and interactive (CLI) tools, and offer basic documentation that covers the usage of the API and the most commonly used native libraries and CLI tools.
* **Lovable** API and SDKs give developers access to almost all GitLab functionality through direct API access, native client libraries and interactive (CLI) tools. Additionally, robust developer documentation is available for all of these options that includes useful resources such as example code and reference applications.

_[Learn more about how GitLab thinks about Maturity here.](/direction/maturity/)_

## What's next and why

Today, developers are leveraging our APIs to integrate GitLab for their needs, but it's important that we better enable them to do more, and add telemetry that helps us understand how they're using those APIs so we can better focus our future development. To this end, our current priorities are:

* **[Create a GitLab Developer Portal](https://gitlab.com/gitlab-org/gitlab/issues/34256)** to make it easier for developers to find the resources that are currently available.
* **[Establish telemetry for our API and SDK](https://gitlab.com/gitlab-org/gitlab/issues/34095)** to allow us to better understand how these tools are being used, and give us insight to which areas need improvement or expansion.
* **Continuing down the path to GraphQL support**
* **Review current community-maintained client libraries** to ensure that they are functional and up to date with the latest features available in our APIs today.

<!--
TODO:
How are we going to measure that
What are we purposefully not doing
-->

## Contributing

At GitLab, one of our values is that everyone can contribute. If you're looking to
contribute your own integration, or otherwise get involved with features in the Ecosystem area, [you can find open issues here](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AAPIs%2FSDKs).

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

## Feedback & Requests

If there's an integration that you'd like to see GitLab offer, or if you have feedback about an existing integration: please [submit an issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new?issue%5Btitle%5D=New%20GitLab%20Integration%20with) with the label `~Category:Integrations`, or contact [Patrick Deuley](mailto:pdeuley@gitlab.com), Sr. Product Manager, Ecosystem.
